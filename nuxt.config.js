export default {

  head: {
    titleTemplate: '%s - grid-architecture',
    title: 'grid-architecture',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  env: {
    BASE_URL: process.env.BASE_URL,
    NODE_ENV: process.env.NODE_ENV
  },

  server: {
    port: 8080, // default: 3000
    host: '0.0.0.0' // default: localhost
  },

  ssr: false,

  css: [
  ],

  plugins: [
    '~/plugins/demo.js',
    '~/plugins/axios.js',
    '~/plugins/notification.js',
    '~/plugins/localStorage.js',
    '~/plugins/cookie.js'
  ],

  components: true,

  buildModules: [
    '@nuxtjs/vuetify'
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy'
  ],

  axios: {},

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      light: true,
      themes: {
        light: {
          primary: '#3f51b5',
          secondary: '#b0bec5',
          anchor: '#8c9eff'
        }
      }
    }
  },

  build: {
  },

  proxy: {
    '/stockapi': {
      target: ' https://www.twse.com.tw/exchangeReport/BWIBBU_ALL?response=open_data/stockapi',
      ws: true,
      changeOrigin: true,
      pathRewrite: {
        '^/api': ''
      }
    }
  }

}
