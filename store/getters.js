export default {
  GetList: (state) => state.list,
  GetLinkModal: (state) => state.linkModal,
  GetColor: (state) => state.color,
}